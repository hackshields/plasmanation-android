package com.arifin.apps.plasmanation.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import static com.arifin.apps.plasmanation.utils.Constant.TAG;


public class PrefManager {

    private final SharedPreferences        sharedPreferences;
    private final SharedPreferences.Editor editor;

    public PrefManager(Context context) {
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
//        sharedPreferences = context.getSharedPreferences("setting", Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
    }

    public Boolean getIsNotification() {
        return sharedPreferences.getBoolean("noti", true);
    }

    public void setIsNotification(Boolean isNotification) {
        editor.putBoolean("noti", isNotification);
        editor.apply();
    }

    public String getPhoneNumber() {
        if (Methods.isEmulator()) {
            return "03001122334";
        } else
            return sharedPreferences.getString("phoneNumber", null);
    }

    public void setPhoneNumber(String phoneNumber) {
        editor.putString("phoneNumber", phoneNumber).apply();
    }

    public boolean isFirstRun() {
        return sharedPreferences.getBoolean("first", true);
    }

    public void setIsFirstRun(boolean yesNo) {
        editor.putBoolean("first", yesNo).apply();
    }

    public int getIsAdsDisabled() {
        return sharedPreferences.getInt("ad_value", 5);
    }

    public void setIsAdsDisabled(int adValue) {
        editor.putInt("ad_value", adValue).apply();
    }

    //    public void saveUserId(String id) {
//        editor.putString("user_id", id).apply();
//    }
    public void saveUserData(String data) {
        editor.putString("user_data", data).apply();

        try {
            if (data != null) {
                JSONObject user = new JSONObject(data);

                setPhoneNumber(user.getString("mobile"));

            } else {
                Log.e(TAG, "saveUserData: null data " + data);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        //.apply();
    }

    //    public String getUserId() {
//        return sharedPreferences.getString("user_id", "");
//    }
    public String getUserData() {
        return sharedPreferences.getString("user_data", "");
    }

    public void clearPref() {
        editor.clear().apply();
    }
}
