package com.arifin.apps.plasmanation.activities;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.TypedValue;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.arifin.apps.plasmanation.R;
import com.arifin.apps.plasmanation.items.ItemAbout;
import com.arifin.apps.plasmanation.utils.Constant;
import com.arifin.apps.plasmanation.utils.DBHelper;
import com.arifin.apps.plasmanation.utils.Methods;
import com.arifin.apps.plasmanation.utils.PrefManager;
import com.arifin.apps.plasmanation.utils.UrlHelper;
import com.google.android.gms.ads.MobileAds;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SplashActivity extends AppCompatActivity {

    private PrefManager pref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
            getWindow().setStatusBarColor(Color.TRANSPARENT);
        }


        Methods methods = new Methods(SplashActivity.this);

        MobileAds.initialize(this, initializationStatus -> {
        });

        pref = new PrefManager(this);

        Resources r       = getResources();
        float     padding = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, Constant.GRID_PADDING, r.getDisplayMetrics());
        Constant.columnWidth  = (int) ((methods.getScreenWidth(this) - ((Constant.NUM_OF_COLUMNS + 1) * padding)) / Constant.NUM_OF_COLUMNS);
        Constant.columnHeight = (int) (Constant.columnWidth * 1.44);


        if (methods.isNetworkAvailable()) {

            if (Methods.check_internet(this)) {
                if (Methods.check_gps(this)) {
                    checkPermissions(true);
                }

                getAboutApp(this);
            }

        } else {
            Constant.showNoNetwork(this);
        }
        checkPermissions(false);


        setRegisteredHandler();
    }

    private void setRegisteredHandler() {
        final int delay = 2000;
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (checkRegistration) {
                    isMobileRegistered(pref.getPhoneNumber());
                } else {
                    new Handler().postDelayed(this, delay);
                }
            }
        }, delay);
    }

    private void getAboutApp(Context context) {
        Methods        methods  = new Methods(context);
        final DBHelper dbHelper = new DBHelper(context);

        if (methods.isNetworkAvailable()) {

            RequestQueue queue = Volley.newRequestQueue(context);
            StringRequest request = new StringRequest(Request.Method.POST, UrlHelper.settingsUrl,
                    response -> {
                        JSONObject jsonObject;
                        try {
                            jsonObject = new JSONObject(response);
                            JSONArray  jsonArray = jsonObject.getJSONArray(Constant.TAG);
                            JSONObject c;
                            for (int i = 0; i < jsonArray.length(); i++) {
                                c = jsonArray.getJSONObject(i);

                                String appname     = c.getString("app_name");
                                String applogo     = c.getString("app_logo");
                                String desc        = c.getString("app_description");
                                String appversion  = c.getString("app_version");
                                String appauthor   = c.getString("app_author");
                                String appcontact  = c.getString("app_contact");
                                String email       = c.getString("app_email");
                                String website     = c.getString("app_website");
                                String privacy     = c.getString("app_privacy_policy");
                                String developedby = c.getString("app_developed_by");
                                Constant.ad_banner_id    = c.getString("banner_ad_id");
                                Constant.ad_inter_id     = c.getString("interstital_ad_id");
                                Constant.isBannerAd      = Boolean.parseBoolean(c.getString("banner_ad"));
                                Constant.isInterAd       = Boolean.parseBoolean(c.getString("interstital_ad"));
                                Constant.ad_publisher_id = c.getString("publisher_id");
                                Constant.adShow          = Integer.parseInt(c.getString("interstital_ad_click"));

                                Constant.itemAbout = new ItemAbout(appname, applogo, desc, appversion, appauthor, appcontact, email, website, privacy, developedby);
//                                    setVariables();
                                dbHelper.addtoAbout();
                            }
                        } catch (Exception ee) {
                            ee.printStackTrace();
                        }
                    },
                    error -> {

                    }
            ) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<>();
                    params.put("", "");
                    return params;
                }
            };

            queue.add(request);
        }
//        else {
//            if (dbHelper.getAbout()) {
//                setVariables();
//            }
//        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (Methods.check_internet(this)) {
            if (Methods.check_gps(this)) {
                checkPermissions(true);
            }
        }
    }

    boolean checkRegistration = false;

    private void checkPermissions(final boolean checkNextAction) {
        Dexter.withActivity(this)
                .withPermissions(
                        Manifest.permission.CALL_PHONE,
                        Manifest.permission.ACCESS_COARSE_LOCATION,
                        Manifest.permission.ACCESS_FINE_LOCATION
                ).withListener(new MultiplePermissionsListener() {

            @Override
            public void onPermissionsChecked(MultiplePermissionsReport report) {
                if (report.areAllPermissionsGranted()) {
                    checkRegistration = checkNextAction;
                }
            }

            @Override
            public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {

                token.continuePermissionRequest();


            }
        }).check();
    }

    private void gotoHome() {
        new Handler().postDelayed(() -> {
            Intent intent = new Intent(SplashActivity.this, MainActivity.class);
            startActivity(intent);
            finish();
        }, 1000);

    }

    private void isMobileRegistered(final String mobile) {

        if (mobile == null) {
            gotoPhoneAuthActivity(false, null);
            return;
        }
        checkUserPhoneNumber(mobile);
    }

    private void checkUserPhoneNumber(final String mobile) {
        try {

            RequestQueue q = Volley.newRequestQueue(this);

            StringRequest request = new StringRequest(Request.Method.POST, UrlHelper.checkUserPhone,
                    response -> {


                        try {
                            JSONObject obj   = new JSONObject(response);
                            JSONArray  array = obj.getJSONArray(Constant.TAG);
                            obj = array.getJSONObject(0);


                            if (obj.getBoolean("isRegistered") && obj.getBoolean("available")) {

                                JSONObject ob = obj.getJSONObject("user_data");
                                pref.saveUserData(ob.toString());

                                if (ob.getString("is_profile_saved").equalsIgnoreCase("1")) {

                                    gotoHome();

                                } else {

                                    gotoPhoneAuthActivity(true, ob.toString());

                                }
                            } else if (!obj.getBoolean("isRegistered") && !obj.getBoolean("available")) {

                                Toast.makeText(SplashActivity.this, R.string.error, Toast.LENGTH_SHORT).show();

                            } else if (!obj.getBoolean("isRegistered") && obj.getBoolean("available")) {

                                JSONObject ob = obj.getJSONObject("user_data");
                                pref.saveUserData(ob.toString());

                                gotoPhoneAuthActivity(true, ob.toString());

                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    },
                    error -> Toast.makeText(SplashActivity.this, R.string.try_again_after_a_while, Toast.LENGTH_SHORT).show()
            ) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<>();
                    params.put("mobile", mobile);
                    return params;
                }
            };

            q.add(request);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void gotoPhoneAuthActivity(boolean toUpdate, String object) {

        Intent intent = new Intent(SplashActivity.this, PhoneAuthActivity.class);
        intent.putExtra("toUpdate", toUpdate);
        intent.putExtra("user_data", object);
        startActivity(intent);
        finish();
    }
}
