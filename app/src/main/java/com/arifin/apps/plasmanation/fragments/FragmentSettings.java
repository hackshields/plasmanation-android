package com.arifin.apps.plasmanation.fragments;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Switch;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.arifin.apps.plasmanation.R;
import com.arifin.apps.plasmanation.utils.PrefManager;
import com.onesignal.OneSignal;

public class FragmentSettings extends Fragment {

    private PrefManager sharedPref;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_settings, container, false);

        sharedPref = new PrefManager(getActivity());

        Boolean isNoti = sharedPref.getIsNotification();

        LinearLayout ll_consent = rootView.findViewById(R.id.ll_consent);
        Switch switch_noti = rootView.findViewById(R.id.switch_noti);
        Switch switch_consent = rootView.findViewById(R.id.switch_consent);

        if (isNoti) {
            switch_noti.setChecked(true);
        } else {
            switch_noti.setChecked(false);
        }

        switch_noti.setOnCheckedChangeListener((buttonView, isChecked) -> {
            Log.e("aaa-noti", "" + isChecked);
//                Toast.makeText(getContext(), "Add one signal subscription", Toast.LENGTH_SHORT).show();
            OneSignal.setSubscription(isChecked);
            sharedPref.setIsNotification(isChecked);
        });

        setHasOptionsMenu(true);

        return rootView;
    }
}
