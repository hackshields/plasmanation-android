package com.arifin.apps.plasmanation.adapters;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.PopupMenu;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.arifin.apps.plasmanation.R;
import com.arifin.apps.plasmanation.activities.AddDonorActivity;
import com.arifin.apps.plasmanation.activities.MainActivity;
import com.arifin.apps.plasmanation.items.Donor;
import com.arifin.apps.plasmanation.utils.Constant;
import com.arifin.apps.plasmanation.utils.UrlHelper;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.arifin.apps.plasmanation.utils.Constant.TAG;

public class AdapterBloodDonors extends RecyclerView.Adapter<AdapterBloodDonors.MyViewHolder> {

    private       boolean          isEdit;
    private       ArrayList<Donor> arrayListFiltered;
    private       ArrayList<Donor> arrayList;
    private       NameFilter       filter;
    private final Context          context;
    boolean horizontal;

    public AdapterBloodDonors(Context context, ArrayList<Donor> arrayList, boolean isEdit, boolean horizontal) {
        this.arrayListFiltered = arrayList;
        this.context           = context;
        this.arrayList         = arrayList;
        this.isEdit            = isEdit;
        this.horizontal        = horizontal;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView;
        if (horizontal)
            itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_donor_horizontal_bloodgroup, parent, false);
        else
            itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_donor_vertical_bloodgroup, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, int position) {

        final Donor donor = arrayListFiltered.get(position);
        holder.name.setText(donor.getFullName());
        holder.blood_group.setText(donor.getBloodGroup());
        holder.time_ago.setText(donor.getTimeAgo());
        holder.city.setText(donor.getCityName());
        holder.distance.setText(donor.getDistance() + " km");
        holder.country.setText(donor.getCountryName());
        holder.type.setText(donor.getType());
        holder.points.setText(donor.getPoints() + " points");
        holder.views.setText(" " + donor.getViews());


        if (isEdit) {

            holder.moreBtn.setOnClickListener(v -> {
                PopupMenu popup = new PopupMenu(context, holder.moreBtn);
                //Inflating the Popup using xml file
                popup.getMenuInflater().inflate(R.menu.donor_edit_popup_menu, popup.getMenu());

                //registering popup with OnMenuItemClickListener
                popup.setOnMenuItemClickListener(item -> {
                    switch (item.getItemId()) {
                        case R.id.edit:

                            Gson gson = new Gson();
                            context.startActivity(new Intent(context, AddDonorActivity.class).putExtra("donorId", donor.getId()).putExtra("donorData", gson.toJson(donor)));

                            break;
                        case R.id.deactivate:
                            ((MainActivity) context).confirmRequest(donor.getId(), "deactivate this donor", "deactivate", "donor");

                            break;
                        case R.id.delete:
                            ((MainActivity) context).confirmRequest(donor.getId(), "delete this donor", "delete", "donor");

                            break;
                    }

                    new Handler().postDelayed(() -> notifyDataSetChanged(), 3000);
                    return true;
                });

                popup.show();
            });
        } else {
            holder.moreBtn.setVisibility(View.GONE);
        }

        holder.itemView.setOnClickListener(v -> {
            showDonorInformation(donor);

            int prevViews = Integer.parseInt(holder.views.getText().toString().replace(" ", ""));
            prevViews++;
            donor.setViews("" + prevViews);
            String viewsNow = " " + prevViews;
            holder.views.setText(viewsNow);
        });
    }


    @Override
    public int getItemCount() {
        return arrayListFiltered.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public Filter getFilter() {
        if (filter == null) {
            filter = new NameFilter();
        }
        return filter;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView name, blood_group, time_ago, city, distance, country, type, points, views;
        ImageView moreBtn;

        private MyViewHolder(View v) {
            super(v);

            name        = v.findViewById(R.id.donorNameText);
            blood_group = v.findViewById(R.id.donorBloodGroupText);
            time_ago    = v.findViewById(R.id.timeAgoText);
            city        = v.findViewById(R.id.donorCityText);
            distance    = v.findViewById(R.id.donorDistanceTextView);
            country     = v.findViewById(R.id.donorCountryTextView);
            type        = v.findViewById(R.id.donorTypeTextView);
            points      = v.findViewById(R.id.donorPointsTextView);
            views       = v.findViewById(R.id.donorViewsCountTextView);
            moreBtn     = v.findViewById(R.id.more);


        }
    }

    private class NameFilter extends Filter {

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {

            constraint = constraint.toString().toLowerCase();
            FilterResults result = new FilterResults();


            Log.e("asdfgh", "performFiltering: constraint: " + constraint.toString() + " city: " + Constant.selectedCity + " bGroup: " + Constant.selectedBgroup);

            if (constraint.toString().equals("") && Constant.selectedCity.length() > 0 && Constant.selectedBgroup.length() > 0) {

                ArrayList<Donor> filteredItems = new ArrayList<>();

                for (int i = 0, l = arrayList.size(); i < l; i++) {
                    Donor dd = arrayList.get(i);


                    if (Constant.selectedCity.equals("Select City") && Constant.selectedBgroup.equals("Select Blood Group")) {
                        filteredItems.add(dd);
                    } else if (Constant.selectedCity.equals("Select City") && !Constant.selectedBgroup.equals("Select Blood Group")) {
                        if (dd.getBloodGroup().equals(Constant.selectedBgroup)) {
                            filteredItems.add(dd);
                        }
                    } else if (Constant.selectedBgroup.equals("Select Blood Group") && !Constant.selectedCity.equals("Select City")) {
                        if (dd.getCity().equals(Constant.selectedCity)) {
                            filteredItems.add(dd);
                        }
                    } else if (dd.getCity().equals(Constant.selectedCity) && dd.getBloodGroup().equals(Constant.selectedBgroup)) {
                        filteredItems.add(dd);
                    }

                }
                result.count  = filteredItems.size();
                result.values = filteredItems;
            } else if (constraint.toString().length() > 0 && Constant.selectedBgroup.length() > 0 && Constant.selectedCity.length() > 0) {
                ArrayList<Donor> filteredItems = new ArrayList<>();
                for (int i = 0, l = arrayList.size(); i < l; i++) {
                    Donor dd = arrayList.get(i);


                    if (dd.getFullName().toLowerCase().contains(constraint)
                            || dd.getMobile().toLowerCase().contains(constraint)
                            || dd.getAddress().toLowerCase().contains(constraint)
                            || dd.getCity().toLowerCase().contains(constraint)
                            || dd.getCountry().toLowerCase().contains(constraint)
                            || dd.getState().toLowerCase().contains(constraint)
                            || dd.getHabits().toLowerCase().contains(constraint)
                    ) {
                        filteredItems.add(dd);
                    }
                    if (Constant.selectedCity.equals("Select City") && Constant.selectedBgroup.equals("Select Blood Group")) {
                        filteredItems.add(dd);
                    } else if (Constant.selectedCity.equals("Select City") && !Constant.selectedBgroup.equals("Select Blood Group")) {
                        if (dd.getBloodGroup().equals(Constant.selectedBgroup)) {
                            filteredItems.add(dd);
                        }
                    } else if (Constant.selectedBgroup.equals("Select Blood Group") && !Constant.selectedCity.equals("Select City")) {
                        if (dd.getCity().equals(Constant.selectedCity)) {
                            filteredItems.add(dd);
                        }
                    } else if (dd.getCity().equals(Constant.selectedCity) && dd.getBloodGroup().equals(Constant.selectedBgroup)) {
                        filteredItems.add(dd);
                    }


//                    }
                }
                result.count  = filteredItems.size();
                result.values = filteredItems;
            } else if (constraint.toString().equals("") && Constant.selectedBgroup.length() > 0) {

                ArrayList<Donor> filteredItems = new ArrayList<>();

                for (int i = 0, l = arrayList.size(); i < l; i++) {
                    Donor dd = arrayList.get(i);


                    if (Constant.selectedBgroup.equals("Select Blood Group")) {
                        filteredItems.add(dd);
                    } else {
                        if (dd.getBloodGroup().equals(Constant.selectedBgroup))
                            filteredItems.add(dd);
                    }
                }
                result.count  = filteredItems.size();
                result.values = filteredItems;
            } else if (constraint.toString().equals("") && Constant.selectedCity.length() > 0) {

                ArrayList<Donor> filteredItems = new ArrayList<>();

                for (int i = 0, l = arrayList.size(); i < l; i++) {
                    Donor dd = arrayList.get(i);


                    if (Constant.selectedCity.equals("Select City")) {
                        filteredItems.add(dd);
                    } else {
                        if (dd.getCity().equals(Constant.selectedCity))
                            filteredItems.add(dd);
                    }
                }
                result.count  = filteredItems.size();
                result.values = filteredItems;
            } else if (constraint.toString().length() > 0 && Constant.selectedBgroup.length() > 0) {
                ArrayList<Donor> filteredItems = new ArrayList<>();

                for (int i = 0, l = arrayList.size(); i < l; i++) {
                    Donor dd = arrayList.get(i);


                    if (dd.getFullName().toLowerCase().contains(constraint)
                            || dd.getMobile().toLowerCase().contains(constraint)
                            || dd.getAddress().toLowerCase().contains(constraint)
                            || dd.getCity().toLowerCase().contains(constraint)
                            || dd.getCountry().toLowerCase().contains(constraint)
                            || dd.getState().toLowerCase().contains(constraint)
                            || dd.getHabits().toLowerCase().contains(constraint)
                            || dd.getLastDonationDate().contains(constraint)
                    )
                        if (Constant.selectedBgroup.equals("Select Blood Group")) {
                            filteredItems.add(dd);
                        } else {
                            if (dd.getBloodGroup().equals(Constant.selectedBgroup))
                                filteredItems.add(dd);
                        }

                }
                result.count  = filteredItems.size();
                result.values = filteredItems;
            } else {
                synchronized (this) {
                    result.values = arrayList;
                    result.count  = arrayList.size();
                }
            }
            return result;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {

            arrayListFiltered = (ArrayList<Donor>) results.values;
            notifyDataSetChanged();
        }
    }


    private void showDonorInformation(final Donor donor) {

        countView(donor.getId());


        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
        dialog.setContentView(R.layout.donors_information_dialog);
        dialog.setCancelable(true);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width  = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;

        final StringBuilder shareText = new StringBuilder();
        final Button        call_btn  = dialog.findViewById(R.id.call_btn);
        final ImageView     close     = dialog.findViewById(R.id.bt_close);
        final ImageView     shareIt   = dialog.findViewById(R.id.bt_share);

        TextView donor_name          = dialog.findViewById(R.id.donor_name);
        TextView donor_type          = dialog.findViewById(R.id.donor_type);
        TextView registered_by       = dialog.findViewById(R.id.registered_by);
        TextView distance            = dialog.findViewById(R.id.distance);
        TextView blood_group         = dialog.findViewById(R.id.blood_group);
        TextView donor_mobile        = dialog.findViewById(R.id.donor_mobile);
        TextView donor_country       = dialog.findViewById(R.id.donor_country);
        TextView donor_state         = dialog.findViewById(R.id.donor_state);
        TextView donor_city          = dialog.findViewById(R.id.donor_city);
        TextView donor_last_donation = dialog.findViewById(R.id.last_donated_date);
        TextView donor_points        = dialog.findViewById(R.id.donor_points);
        TextView donor_views         = dialog.findViewById(R.id.donor_views);
        TextView donor_habits        = dialog.findViewById(R.id.donor_habits);
        TextView donor_address       = dialog.findViewById(R.id.donor_address);
        TextView donor_member_since  = dialog.findViewById(R.id.member_since);


        donor_name.setText(donor.getFullName());
        donor_type.setText(donor.getType());
        registered_by.setText(donor.getAddedBy());
        distance.setText(donor.getDistance());
        blood_group.setText(donor.getBloodGroup());
        donor_mobile.setText(donor.getMobile());
        donor_country.setText(donor.getCountryName());
        donor_state.setText(donor.getStateName());
        donor_city.setText(donor.getCityName());
        donor_last_donation.setText(donor.getLastDonationDate());
        donor_points.setText(donor.getPoints());
        donor_views.setText(donor.getViews());
        donor_habits.setText(donor.getHabits());
        donor_address.setText(donor.getAddress());
        donor_member_since.setText(donor.getCreated());


        shareText.append("Donor:  ").append(donor.getFullName()).append("\n");
        shareText.append("Type:   ").append(donor.getType()).append("\n");
        shareText.append("Blood Group: ").append(donor.getBloodGroup()).append("\n");
        shareText.append("Mobile:  ").append(donor.getMobile()).append("\n");
        shareText.append("Region: ").append(donor.getCityName()).append(", ").append(donor.getStateName()).append(", ").append(donor.getCountryName()).append("\n");
        shareText.append("Address: ").append(donor.getAddress()).append("\n");
        shareText.append("Last Donation: ").append(donor.getLastDonationDate()).append("\n");

        close.setOnClickListener(v -> dialog.dismiss());


        shareIt.setOnClickListener(v -> {

            Intent sendIntent = new Intent();
            sendIntent.setAction(Intent.ACTION_SEND);
            sendIntent.putExtra(Intent.EXTRA_TITLE, "Share with");
            sendIntent.putExtra(Intent.EXTRA_TEXT, shareText.toString());
            sendIntent.setType("text/plain");

            Intent shareIntent = Intent.createChooser(sendIntent, null);
            context.startActivity(shareIntent);
        });

        call_btn.setOnClickListener(view -> {

            ((MainActivity) context).CallToDonorPHone(donor.getMobile(), donor.getId());
            dialog.dismiss();
        });

        dialog.show();
        dialog.getWindow().

                setAttributes(lp);
    }

    private void countView(final String id) {
        RequestQueue q = Volley.newRequestQueue(context);
        StringRequest request = new StringRequest(Request.Method.POST,
                UrlHelper.countViewUrl, response -> Log.d(TAG, "onResponse: " + response), error -> Log.d(TAG, "onErrorResponse: " + error.toString())) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                HashMap<String, String> params = new HashMap<>();
                params.put("id", id);
                params.put("type", "donor");
                return params;
            }
        };

        q.add(request);
    }


}