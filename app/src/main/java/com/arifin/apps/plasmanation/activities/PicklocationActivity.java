package com.arifin.apps.plasmanation.activities;

import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.view.inputmethod.InputMethodManager;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.arifin.apps.plasmanation.R;
import com.arifin.apps.plasmanation.items.PlaceAutoCompleteItem;
import com.arifin.apps.plasmanation.utils.Constant;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.Builder;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;

//import com.otacodes.goestateapp.Constants.C1426Constants;
//import com.otacodes.goestateapp.Item.PlaceAutoCompleteItem;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import io.github.inflationx.viewpump.ViewPumpContextWrapper;


public class PicklocationActivity extends AppCompatActivity implements OnMapReadyCallback, ConnectionCallbacks, OnConnectionFailedListener {
    public static final String FORM_VIEW_INDICATOR = "FormToFill";
    public static final String LOCATION_LATLNG = "LocationLatLng";
    public static final String LOCATION_NAME = "LocationName";
    public static final int LOCATION_PICKER_ID = 78;
    private static final int REQUEST_PERMISSION_LOCATION = 991;
    AutoCompleteTextView autoCompleteTextView;
    ImageView backbutton;
    TextView currentAddress;
    private int formToFill;
    public GoogleMap gMap;
    private GoogleApiClient googleApiClient;
    private Location lastKnownLocation;
    public PlaceAutoCompleteItem mAdapter;
    Button selectLocation;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase));
    }

    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
    }

    public void onConnectionSuspended(int i) {
    }

    public void onStart() {
        super.onStart();
        this.googleApiClient.connect();
    }

    public void onStop() {
        super.onStop();
        this.googleApiClient.disconnect();
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_picklocation);
        this.autoCompleteTextView = findViewById(R.id.locationPicker_autoCompleteText);
        this.currentAddress = findViewById(R.id.locationPicker_currentAddress);
        this.selectLocation = findViewById(R.id.locationPicker_destinationButton);
        this.backbutton = findViewById(R.id.back_btn);
        setupGoogleApiClient();
        ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.locationPicker_maps)).getMapAsync(this);
        setupAutocompleteTextView();
        this.formToFill = getIntent().getIntExtra(FORM_VIEW_INDICATOR, -1);
        this.selectLocation.setOnClickListener(view -> PicklocationActivity.this.selectLocation());
        this.backbutton.setOnClickListener(view -> PicklocationActivity.this.finish());
    }

    public void selectLocation() {
        LatLng latLng = this.gMap.getCameraPosition().target;
        String charSequence = this.currentAddress.getText().toString();
        Intent intent = new Intent();
        intent.putExtra(FORM_VIEW_INDICATOR, this.formToFill);
        intent.putExtra(LOCATION_NAME, charSequence);
        intent.putExtra(LOCATION_LATLNG, latLng);
        setResult(-1, intent);
        finish();
    }

    private void setupGoogleApiClient() {
        if (this.googleApiClient == null) {
            this.googleApiClient = new Builder(this).addConnectionCallbacks(this).addOnConnectionFailedListener(this).addApi(LocationServices.API).addApi(Places.GEO_DATA_API).build();
        }
    }

    private void setupAutocompleteTextView() {
        this.mAdapter = new PlaceAutoCompleteItem(this, this.googleApiClient, Constant.BOUNDS, null);
        this.autoCompleteTextView.setAdapter(this.mAdapter);
        this.autoCompleteTextView.setOnItemClickListener((adapterView, view, i, j) -> {
            ((InputMethodManager) PicklocationActivity.this.getSystemService(INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(PicklocationActivity.this.autoCompleteTextView.getWindowToken(), 2);
            PicklocationActivity.this.getLocationFromPlaceId(PicklocationActivity.this.mAdapter.getItem(i).getPlaceId(), placeBuffer -> {
                if (placeBuffer.getStatus().isSuccess()) {
                    PicklocationActivity.this.gMap.moveCamera(CameraUpdateFactory.newLatLng(placeBuffer.get(0).getLatLng()));
                }
            });
        });
    }

    public void getLocationFromPlaceId(String str, ResultCallback<PlaceBuffer> resultCallback) {
        Places.GeoDataApi.getPlaceById(this.googleApiClient, str).setResultCallback(resultCallback);
    }

    private void updateLastLocation() {
        if (ActivityCompat.checkSelfPermission(this, "android.permission.ACCESS_FINE_LOCATION") != 0) {
            ActivityCompat.requestPermissions(this, new String[]{"android.permission.ACCESS_FINE_LOCATION"}, REQUEST_PERMISSION_LOCATION);
            return;
        }
        this.lastKnownLocation = LocationServices.FusedLocationApi.getLastLocation(this.googleApiClient);
        this.gMap.setMyLocationEnabled(true);
        if (this.lastKnownLocation != null) {
            this.gMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(this.lastKnownLocation.getLatitude(), this.lastKnownLocation.getLongitude()), 15.0f));
            this.gMap.animateCamera(CameraUpdateFactory.zoomTo(15.0f));
        }
    }

    public void onMapReady(GoogleMap googleMap) {
        this.gMap = googleMap;
        this.gMap.getUiSettings().setMyLocationButtonEnabled(true);
        updateLastLocation();
        setupMapOnCameraChange();
    }

    public void onRequestPermissionsResult(int i, @NonNull String[] strArr, @NonNull int[] iArr) {
        super.onRequestPermissionsResult(i, strArr, iArr);
        if (i == REQUEST_PERMISSION_LOCATION && iArr.length > 0 && iArr[0] == 0) {
            updateLastLocation();
        }
    }

    private void setupMapOnCameraChange() {
        this.gMap.setOnCameraIdleListener(() -> PicklocationActivity.this.fillAddress(PicklocationActivity.this.currentAddress, PicklocationActivity.this.gMap.getCameraPosition().target));
    }

    public void fillAddress(final TextView textView, final LatLng latLng) {
        new Thread(() -> {
            try {
                final List fromLocation = new Geocoder(PicklocationActivity.this, Locale.getDefault()).getFromLocation(latLng.latitude, latLng.longitude, 1);
                PicklocationActivity.this.runOnUiThread(() -> {
                    if (fromLocation.isEmpty()) {
                        textView.setText("not Availeble");
                    } else if (fromLocation.size() > 0) {
                        textView.setText(((Address) fromLocation.get(0)).getAddressLine(0));
                    }
                });
            } catch (IOException e) {
                e.printStackTrace();
            }
        }).start();
    }

    public void onConnected(@Nullable Bundle bundle) {
        updateLastLocation();
    }
}
