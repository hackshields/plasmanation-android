package com.arifin.apps.plasmanation.utils;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.arifin.apps.plasmanation.items.ItemAbout;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class DBHelper extends SQLiteOpenHelper {

    private static final String DB_NAME = "bloodbank.db";
    private final Context context;
    private SQLiteDatabase db;
    private final String DB_PATH;

    public DBHelper(Context context) {
        super(context, DB_NAME, null, 3);
        this.context = context;
        DB_PATH = "/data/data/" + context.getPackageName() + "/" + "databases/";
    }

    public void createDataBase() throws IOException {

        boolean dbExist = checkDataBase();
        if (!dbExist) {
            getWritableDatabase();
            copyDataBase();
        } else {
            this.getWritableDatabase();
        }
    }

    private boolean checkDataBase() {
        File dbFile = new File(DB_PATH + DB_NAME);
        return dbFile.exists();
    }

    private void copyDataBase() throws IOException {

        InputStream myInput = context.getAssets().open(DB_NAME);
        String outFileName = DB_PATH + DB_NAME;
        OutputStream myOutput = new FileOutputStream(outFileName);
        byte[] buffer = new byte[1024];
        int length;
        while ((length = myInput.read(buffer)) > 0) {
            myOutput.write(buffer, 0, length);
        }

        // Close the streams
        myOutput.flush();
        myOutput.close();
        myInput.close();
    }

    private Cursor getData(String Query) {
        String myPath = DB_PATH + DB_NAME;
        Cursor c = null;
        try {
            db = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READWRITE);
            c = db.rawQuery(Query, null);
        } catch (Exception e) {
            Log.e("Err", e.toString());
        }
        return c;
    }

    //UPDATE temp_dquot SET age='20',name1='--',rdt='11/08/2014',basic_sa='100000',plno='814',pterm='20',mterm='20',mat_date='11/08/2034',mode='YLY',dab_sa='100000',tr_sa='0',cir_sa='',bonus_rate='42',prem='5276',basic_prem='5118',dab_prem='100.0',step_rate='for Life',loyal_rate='0',bonus_rate='42',act_mat='1,88,000',mly_b_pr='448',qly_b_pr='1345',hly_b_pr='2664',yly_b_pr='5276'  WHERE uniqid=1
    private void dml(String Query) {
        String myPath = DB_PATH + DB_NAME;
        if (db == null)
            db = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READWRITE);
        try {
            db.execSQL(Query);
        } catch (Exception e) {
            Log.e("Error", e.toString());
        }
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
    }

    public void addtoAbout() {
        try {
            dml("delete from about");
            dml("insert into about (name,logo,version,author,contact,email,website,desc,developed,privacy, ad_pub, ad_banner, ad_inter, isbanner, isinter, click) values (" +
                    "'" + Constant.itemAbout.getAppName() + "','" + Constant.itemAbout.getAppLogo() + "','" + Constant.itemAbout.getAppVersion() + "'" +
                    ",'" + Constant.itemAbout.getAuthor() + "','" + Constant.itemAbout.getContact() + "','" + Constant.itemAbout.getEmail() + "'" +
                    ",'" + Constant.itemAbout.getWebsite() + "','" + Constant.itemAbout.getAppDesc() + "','" + Constant.itemAbout.getDevelopedby() + "'" +
                    ",'" + Constant.itemAbout.getPrivacy() + "','" + Constant.ad_publisher_id + "','" + Constant.ad_banner_id + "','" + Constant.ad_inter_id + "'" +
                    ",'" + Constant.isBannerAd + "','" + Constant.isInterAd + "','" + Constant.adShow + "')");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Boolean getAbout() {
        try {
            String selectQuery = "SELECT * FROM about";

            Cursor c = getData(selectQuery);

            if (c != null && c.getCount() > 0) {
                c.moveToFirst();
                for (int i = 0; i < c.getCount(); i++) {
                    String appname = c.getString(c.getColumnIndex("name"));
                    String applogo = c.getString(c.getColumnIndex("logo"));
                    String desc = c.getString(c.getColumnIndex("desc"));
                    String appversion = c.getString(c.getColumnIndex("version"));
                    String appauthor = c.getString(c.getColumnIndex("author"));
                    String appcontact = c.getString(c.getColumnIndex("contact"));
                    String email = c.getString(c.getColumnIndex("email"));
                    String website = c.getString(c.getColumnIndex("website"));
                    String privacy = c.getString(c.getColumnIndex("privacy"));
                    String developedby = c.getString(c.getColumnIndex("developed"));

                    Constant.ad_banner_id    = c.getString(c.getColumnIndex("ad_banner"));
                    Constant.ad_inter_id     = c.getString(c.getColumnIndex("ad_inter"));
                    Constant.isBannerAd      = Boolean.parseBoolean(c.getString(c.getColumnIndex("isbanner")));
                    Constant.isInterAd       = Boolean.parseBoolean(c.getString(c.getColumnIndex("isinter")));
                    Constant.ad_publisher_id = c.getString(c.getColumnIndex("ad_pub"));
                    Constant.adShow          = Integer.parseInt(c.getString(c.getColumnIndex("click")));

                    Constant.itemAbout = new ItemAbout(appname, applogo, desc, appversion, appauthor, appcontact, email, website, privacy, developedby);
                }
                c.close();
                return true;
            } else {
                return false;
            }
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.e("aaa", "upgrade");
        Log.e("aaa -oldVersion", "" + oldVersion);
        Log.e("aaa -newVersion", "" + newVersion);
        try {
            if (db == null) {
                String myPath = DB_PATH + DB_NAME;
                db = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READWRITE);
            }
            switch (oldVersion) {
                case 1:
                case 2:

                    db.execSQL("ALTER TABLE about ADD 'ad_pub' TEXT");
                    db.execSQL("ALTER TABLE about ADD 'ad_banner' TEXT");
                    db.execSQL("ALTER TABLE about ADD 'ad_inter' TEXT");
                    db.execSQL("ALTER TABLE about ADD 'isbanner' TEXT");
                    db.execSQL("ALTER TABLE about ADD 'isinter' TEXT");
                    db.execSQL("ALTER TABLE about ADD 'click' TEXT");

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

//    public void addToCities(List<CityModel> cities) {
//        try {
//            dml("delete from cities");
//
//            for (int i = 0; i < cities.size(); i++) {
//                dml("insert into cities (id, city, latitude, longitude) values ('" + cities.get(i).getId() + "','" + cities.get(i).getName() + "','" + cities.get(i).getLat() + "','" + cities.get(i).getLon() + "')");
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }


//    public ArrayList<CityModel> getCities() {
//        ArrayList<CityModel> arrayList = new ArrayList<>();
//
//        Cursor cursor = getData("select * from cities");
//        if (cursor != null && cursor.getCount() > 0) {
//            cursor.moveToFirst();
//            for (int i = 0; i < cursor.getCount(); i++) {
//                String cid = cursor.getString(cursor.getColumnIndex("id"));
//                String city = cursor.getString(cursor.getColumnIndex("city"));
//                String lat = cursor.getString(cursor.getColumnIndex("latitude"));
//                String lon = cursor.getString(cursor.getColumnIndex("longitude"));
//
//                CityModel itemCategory = new CityModel(cid, city, lat, lon);
//                arrayList.add(itemCategory);
//
//                cursor.moveToNext();
//            }
//            cursor.close();
//        }
//        return arrayList;
//    }
}