package com.arifin.apps.plasmanation.items;

import android.content.Context;
import android.text.style.CharacterStyle;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;
import android.widget.Toast;

import com.arifin.apps.plasmanation.R;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.data.DataBufferUtils;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.AutocompletePrediction;
import com.google.android.gms.location.places.AutocompletePredictionBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.model.LatLngBounds;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

//import com.google.android.gms.location.places.AutocompleteFilter;
//import com.google.android.gms.location.places.AutocompletePrediction;
//import com.google.android.gms.location.places.AutocompletePredictionBuffer;
//import com.google.android.gms.location.places.Places;
//import com.google.android.gms.location.places.GeoDataApi;
//import com.google.android.libraries.places.AutocompleteFilter;
//import com.google.android.libraries.places.compat.AutocompletePrediction;
//import com.google.android.libraries.places.compat.AutocompletePredictionBuffer;

//import android.widget.Filter.FilterResults;

public class PlaceAutoCompleteItem extends ArrayAdapter<AutocompletePrediction> implements Filterable {
    private static final CharacterStyle STYLE_BOLD = new StyleSpan(1);
    private static final String TAG = "PlaceAutocomplete";
    private LatLngBounds mBounds;
    private GoogleApiClient mGoogleApiClient;
    private AutocompleteFilter mPlaceFilter;
    /* access modifiers changed from: private */
    public ArrayList<AutocompletePrediction> mResultList;

    public PlaceAutoCompleteItem(Context context, GoogleApiClient googleApiClient, LatLngBounds latLngBounds, AutocompleteFilter autocompleteFilter) {
//        super(context, 17367047, 16908308);
        super(context, R.layout.place_autocomplete_item_prediction, R.id.place_autocomplete_prediction_primary_text);
        this.mGoogleApiClient = googleApiClient;
        this.mBounds = latLngBounds;
        this.mPlaceFilter = autocompleteFilter;
    }

    public void setBounds(LatLngBounds latLngBounds) {
        this.mBounds = latLngBounds;
    }

    public int getCount() {
        return this.mResultList.size();
    }

    public AutocompletePrediction getItem(int i) {
        return this.mResultList.get(i);
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        View view2 = super.getView(i, view, viewGroup);
        AutocompletePrediction item = getItem(i);
        TextView textView = view2.findViewById(R.id.place_autocomplete_prediction_secondary_text);
        ((TextView) view2.findViewById(R.id.place_autocomplete_prediction_primary_text)).setText(item.getPrimaryText(STYLE_BOLD));
        textView.setText(item.getSecondaryText(STYLE_BOLD));
        return view2;
    }

    public Filter getFilter() {
        return new Filter() {
            /* access modifiers changed from: protected */
            public FilterResults performFiltering(CharSequence charSequence) {
                FilterResults filterResults = new FilterResults();
                if (charSequence != null) {
                    PlaceAutoCompleteItem.this.mResultList = PlaceAutoCompleteItem.this.getAutocomplete(charSequence);
                    if (PlaceAutoCompleteItem.this.mResultList != null) {
                        filterResults.values = PlaceAutoCompleteItem.this.mResultList;
                        filterResults.count = PlaceAutoCompleteItem.this.mResultList.size();
                    }
                }
                return filterResults;
            }

            /* access modifiers changed from: protected */
            public void publishResults(CharSequence charSequence, FilterResults filterResults) {
                if (filterResults == null || filterResults.count <= 0) {
                    PlaceAutoCompleteItem.this.notifyDataSetInvalidated();
                } else {
                    PlaceAutoCompleteItem.this.notifyDataSetChanged();
                }
            }

            public CharSequence convertResultToString(Object obj) {
                if (obj instanceof AutocompletePrediction) {
                    return ((AutocompletePrediction) obj).getFullText(null);
                }
                return super.convertResultToString(obj);
            }
        };
    }

    /* access modifiers changed from: private */
    public ArrayList<AutocompletePrediction> getAutocomplete(CharSequence charSequence) {
        if (this.mGoogleApiClient.isConnected()) {
            String str = TAG;
            StringBuilder sb = new StringBuilder();
            sb.append("Query for: ");
            sb.append(charSequence);
            Log.i(str, sb.toString());
//            AutocompletePredictionBuffer autocompletePredictionBuffer = (AutocompletePredictionBuffer) Places.GeoDataApi.getAutocompletePredictions(this.mGoogleApiClient, charSequence.toString(), this.mBounds, this.mPlaceFilter).await(60, TimeUnit.SECONDS);
//            AutocompletePredictionBuffer autocompletePredictionBuffer = Places.GeoDataApi.getAutocompletePredictions(this.mGoogleApiClient, charSequence.toString(), this.mBounds, this.mPlaceFilter).await(60, TimeUnit.SECONDS);
//            AutocompletePredictionBuffer autocompletePredictionBuffer = GeodataApi.getAutocompletePredictions(this.mGoogleApiClient, charSequence.toString(), this.mBounds, this.mPlaceFilter).await(60, TimeUnit.SECONDS);
            PendingResult<AutocompletePredictionBuffer> results = Places.GeoDataApi.getAutocompletePredictions(mGoogleApiClient, charSequence.toString(), this.mBounds, this.mPlaceFilter);//.await(60, TimeUnit.SECONDS);
            AutocompletePredictionBuffer autocompletePredictionBuffer = results.await(60, TimeUnit.SECONDS);
//            AutocompletePredictionBuffer autocompletePredictionBuffer = (AutocompletePredictionBuffer) Places.getGeoDataClient(getContext()).getAutocompletePredictions(this.mGoogleApiClient, charSequence.toString(), this.mBounds, this.mPlaceFilter).await(60, TimeUnit.SECONDS);
            Status status = autocompletePredictionBuffer.getStatus();
            if (!status.isSuccess()) {
                Context context = getContext();
                StringBuilder sb2 = new StringBuilder();
                sb2.append("Error contacting API: ");
                sb2.append(status.toString());
                Toast.makeText(context, sb2.toString(), Toast.LENGTH_SHORT).show();
                String str2 = TAG;
                StringBuilder sb3 = new StringBuilder();
                sb3.append("Error getting autocomplete prediction API call: ");
                sb3.append(status.toString());
                Log.e(str2, sb3.toString());
                autocompletePredictionBuffer.release();
                return null;
            }
            String str3 = TAG;
            StringBuilder sb4 = new StringBuilder();
            sb4.append("Query completed. Received ");
            sb4.append(autocompletePredictionBuffer.getCount());
            sb4.append(" predictions.");
            Log.i(str3, sb4.toString());
            return DataBufferUtils.freezeAndClose(autocompletePredictionBuffer);
        }
        Log.e(TAG, "Google API client is not connected for autocomplete query.");
        return null;
    }
}
