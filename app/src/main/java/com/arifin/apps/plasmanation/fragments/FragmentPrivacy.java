package com.arifin.apps.plasmanation.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.arifin.apps.plasmanation.R;
import com.arifin.apps.plasmanation.activities.MainActivity;
import com.arifin.apps.plasmanation.utils.Constant;
import com.arifin.apps.plasmanation.utils.DBHelper;
import com.arifin.apps.plasmanation.utils.Methods;

public class FragmentPrivacy extends Fragment {

    private WebView  webView;
    private DBHelper dbHelper;
    private Context  ctx;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_privacy, container, false);

        ctx = getActivity();
        handleAbout(rootView);

        return rootView;
    }

    private void handleAbout(View v) {
        dbHelper = new DBHelper(ctx);
        Methods methods = new Methods(ctx);
        methods.setStatusColor(((MainActivity) ctx).getWindow());
        methods.forceRTLIfSupported(((MainActivity) ctx).getWindow());

        ProgressDialog pbar = new ProgressDialog(ctx);
        pbar.setMessage(getResources().getString(R.string.loading));
        pbar.setCancelable(false);

        webView = v.findViewById(R.id.webView);
        setVariables();
    }

    private void setVariables() {

        String content = "<?xml version='1.0' encoding='UTF-8' ?>" +
                "<html><head>" +
                "<meta http-equiv='content-type' content='text/html; charset=utf-8' />" +
                "</head><body>" + Constant.itemAbout.getPrivacy() + "</body></html>";

        Log.e("asdf", content);

        String encodedHtml = Base64.encodeToString(content.getBytes(), Base64.NO_PADDING);
        webView.loadData(encodedHtml, "text/html", "base64");

    }
}
