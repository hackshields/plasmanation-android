package com.arifin.apps.plasmanation.activities;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.arifin.apps.plasmanation.R;
import com.arifin.apps.plasmanation.fragments.FragmentAbout;
import com.arifin.apps.plasmanation.fragments.FragmentBlog;
import com.arifin.apps.plasmanation.fragments.FragmentBlogDesc;
import com.arifin.apps.plasmanation.fragments.FragmentBloodBanksHome;
import com.arifin.apps.plasmanation.fragments.FragmentCalls;
import com.arifin.apps.plasmanation.fragments.FragmentDonorHome;
import com.arifin.apps.plasmanation.fragments.FragmentPrivacy;
import com.arifin.apps.plasmanation.fragments.FragmentProfile;
import com.arifin.apps.plasmanation.fragments.FragmentReminder;
import com.arifin.apps.plasmanation.fragments.FragmentRequestsHome;
import com.arifin.apps.plasmanation.fragments.FragmentSettings;
import com.arifin.apps.plasmanation.items.ItemCallRate;
import com.arifin.apps.plasmanation.utils.Constant;
import com.arifin.apps.plasmanation.utils.Methods;
import com.arifin.apps.plasmanation.utils.PrefManager;
import com.arifin.apps.plasmanation.utils.UrlHelper;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.github.inflationx.viewpump.ViewPumpContextWrapper;

import static com.android.volley.VolleyLog.TAG;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    public  FragmentManager      fm;
    public  JSONObject           userData;
    private PrefManager          pref;
    private BottomNavigationView bottomNavView;

    private Button add_donor, add_blood_request;
    private Methods methods;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        fm      = getSupportFragmentManager();
        methods = new Methods(true, this);

        get_user_data();

        initViews();

        setUpListeners();

        bottomNavView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        setupDrawerNavigation(toolbar);


        loadDonorHome();


        loadAdsIf();
    }

    private void loadAdsIf() {
        if (Constant.check_admob) {
            if (methods.isNetworkAvailable()) {
                methods.loadInterAds(this);
            }
//            else {
//                findViewById(R.id.ll_ad).setVisibility(View.GONE);
//            }
        }
//        else {
//            findViewById(R.id.ll_ad).setVisibility(View.GONE);
//        }
    }

    private void setUpListeners() {
        add_donor.setOnClickListener(v -> {
            startActivity(new Intent(MainActivity.this, AddDonorActivity.class));
            methods.showInterAd(MainActivity.this);

        });

        add_blood_request.setOnClickListener(v -> {
            startActivity(new Intent(MainActivity.this, AddRequestActivity.class));
            methods.showInterAd(MainActivity.this);

        });
    }

    private void loadDonorHome() {
        loadFrag(new FragmentDonorHome(), getResources().getString(R.string.title_donors), fm, false);
        if (getSupportActionBar() != null)
            getSupportActionBar().setTitle(getResources().getString(R.string.title_donors));

        add_donor.setVisibility(View.VISIBLE);
        add_blood_request.setVisibility(View.GONE);

    }

    public void loadBlogDetails() {
        loadFrag(new FragmentBlogDesc(), getResources().getString(R.string.blog_description), fm, false);
        if (getSupportActionBar() != null)
            getSupportActionBar().setTitle(getResources().getString(R.string.blog_description));

        add_donor.setVisibility(View.GONE);
        add_blood_request.setVisibility(View.GONE);

    }

    private void loadRemindersFragment() {
        loadFrag(new FragmentReminder(), getResources().getString(R.string.reminder), fm, false);
        if (getSupportActionBar() != null)
            getSupportActionBar().setTitle(getResources().getString(R.string.reminder));

        add_donor.setVisibility(View.GONE);
        add_blood_request.setVisibility(View.GONE);

    }

    private void loadCallsFragment() {
        loadFrag(new FragmentCalls(), getResources().getString(R.string.calls), fm, false);
        if (getSupportActionBar() != null)
            getSupportActionBar().setTitle(getResources().getString(R.string.calls));

        add_donor.setVisibility(View.GONE);
        add_blood_request.setVisibility(View.GONE);

    }

    private void loadAboutFragment() {
        loadFrag(new FragmentAbout(), getResources().getString(R.string.menu_about), fm, false);
        if (getSupportActionBar() != null)
            getSupportActionBar().setTitle(getResources().getString(R.string.menu_about));

        add_donor.setVisibility(View.GONE);
        add_blood_request.setVisibility(View.GONE);
    }

    private void loadPrivacyFragment() {
        loadFrag(new FragmentPrivacy(), getResources().getString(R.string.menu_privacy_policy), fm, true);
        if (getSupportActionBar() != null)
            getSupportActionBar().setTitle(getResources().getString(R.string.menu_privacy_policy));

        add_donor.setVisibility(View.GONE);
        add_blood_request.setVisibility(View.GONE);
    }


    private void loadSettingsFragment() {
        loadFrag(new FragmentSettings(), getResources().getString(R.string.settings), fm, false);
        if (getSupportActionBar() != null)
            getSupportActionBar().setTitle(getResources().getString(R.string.settings));

        add_donor.setVisibility(View.GONE);
        add_blood_request.setVisibility(View.GONE);

    }

    private void setupDrawerNavigation(Toolbar toolbar) {
        DrawerLayout   drawer         = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);
    }

    private void get_user_data() {
        pref = new PrefManager(this);
        try {
            userData = new JSONObject(pref.getUserData());
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void initViews() {
        bottomNavView = findViewById(R.id.bottom_nav_view);

        add_donor         = findViewById(R.id.add_donor);
        add_blood_request = findViewById(R.id.add_request);

    }

    public void loadFrag(Fragment f1, String name, FragmentManager fm, boolean addToBackStack) {
        FragmentTransaction ft = fm.beginTransaction();
        if (name.equals(getResources().getString(R.string.title_donors))) {
            addToBackStack = false;
            fm.popBackStackImmediate();
            fm.popBackStack();
        }
        if (addToBackStack)
            ft.addToBackStack(name);

        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        ft.replace(R.id.frame_layout, f1, name);
        ft.commit();
        if (getSupportActionBar() != null)
            getSupportActionBar().setTitle(name);
    }


    public void setActionTitle(String title) {
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(title);
        }
    }

    @Override
    public void onBackPressed() {

        bottomNavView.setVisibility(View.VISIBLE);

        if (!getSupportActionBar().getTitle().equals(getResources().getString(R.string.title_donors))) {
            loadDonorHome();

            bottomNavView.setSelectedItemId(R.id.nav_donors_home);

        } else {


            DrawerLayout drawer = findViewById(R.id.drawer_layout);
            if (drawer.isDrawerOpen(GravityCompat.START)) {
                drawer.closeDrawer(GravityCompat.START);

            } else {
                super.onBackPressed();
            }
        }

    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        methods.showInterAd(MainActivity.this);

        if (id == R.id.nav_home) {
            loadDonorHome();
        } else if (id == R.id.nav_reminder) {
            loadRemindersFragment();
        } else if (id == R.id.nav_calls) {
            loadCallsFragment();
        } else if (id == R.id.nav_about) {
            loadAboutFragment();
            add_donor.setVisibility(View.GONE);
            add_blood_request.setVisibility(View.GONE);
        } else if (id == R.id.nav_privacy_policy) {
//            openPrivacyDialog();
            loadPrivacyFragment();
        } else if (id == R.id.nav_share) {
            Toast.makeText(this, R.string.share_app, Toast.LENGTH_SHORT).show();
            Intent sendIntent = new Intent();
            sendIntent.setAction(Intent.ACTION_SEND);
            sendIntent.putExtra(Intent.EXTRA_TEXT, getString(R.string.share_app_text) + " " + " https://play.google.com/store/apps/details?id=" + getPackageName());
            sendIntent.setType("text/plain");
            Intent shareIntent = Intent.createChooser(sendIntent, null);
            startActivity(shareIntent);
        } else if (id == R.id.nav_sign_out) {
            signOut();
        } else if (id == R.id.nav_settings) {
            loadSettingsFragment();
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {

            methods.showInterAd(MainActivity.this);

            switch (item.getItemId()) {
                case R.id.nav_donors_home:

                    loadDonorHome();

                    return true;
                case R.id.nav_blood_requests:

                    loadRequestHome();

                    return true;
                case R.id.nav_blood_banks:

                    loadBloodBanksHome();

                    return true;
                case R.id.nav_profile:
                    add_donor.setVisibility(View.GONE);
                    add_blood_request.setVisibility(View.GONE);
                    loadProfileHome();
                    return true;
                case R.id.nav_blog:
                    loadBlogFragment();
                    add_donor.setVisibility(View.GONE);
                    add_blood_request.setVisibility(View.GONE);
                    return true;
            }
            return false;
        }
    };

    private void loadBloodBanksHome() {
        loadFrag(new FragmentBloodBanksHome(), getResources().getString(R.string.title_blood_banks), fm, false);
        if (getSupportActionBar() != null)
            getSupportActionBar().setTitle(getResources().getString(R.string.title_blood_banks));

        add_donor.setVisibility(View.GONE);
        add_blood_request.setVisibility(View.GONE);
    }

    private void loadProfileHome() {
        loadFrag(new FragmentProfile(), getResources().getString(R.string.title_profile), fm, false);
        if (getSupportActionBar() != null)
            getSupportActionBar().setTitle(getResources().getString(R.string.title_profile));

        add_donor.setVisibility(View.GONE);
        add_blood_request.setVisibility(View.GONE);
    }

    private void loadBlogFragment() {
        loadFrag(new FragmentBlog(), getResources().getString(R.string.title_blog), fm, false);
        if (getSupportActionBar() != null)
            getSupportActionBar().setTitle(getResources().getString(R.string.title_blog));

        add_donor.setVisibility(View.GONE);
        add_blood_request.setVisibility(View.GONE);
    }

    private void loadRequestHome() {
        loadFrag(new FragmentRequestsHome(), getResources().getString(R.string.title_blood_requests), fm, false);
        if (getSupportActionBar() != null)
            getSupportActionBar().setTitle(getResources().getString(R.string.title_blood_requests));

        add_donor.setVisibility(View.GONE);
        add_blood_request.setVisibility(View.VISIBLE);
    }


    public void CallToPHone(String phone) {
//        Toast.makeText(this, "implement ads", Toast.LENGTH_SHORT).show();

        Intent callIntent = new Intent(Intent.ACTION_CALL);
        callIntent.setData(Uri.parse("tel:" + phone));
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {

            checkPermissions(callIntent);
            return;
        }
        startActivity(callIntent);
    }


    private void checkPermissions(final Intent callIntentAfterGrant) {
        Dexter.withActivity(this)
                .withPermissions(
                        Manifest.permission.CALL_PHONE,
                        Manifest.permission.ACCESS_COARSE_LOCATION,
                        Manifest.permission.ACCESS_FINE_LOCATION
                ).withListener(new MultiplePermissionsListener() {

            @Override
            public void onPermissionsChecked(MultiplePermissionsReport report) {
                if (report.areAllPermissionsGranted()) {
                    startActivity(callIntentAfterGrant);
                }
            }

            @Override
            public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {

                token.continuePermissionRequest();

            }
        }).check();
    }

    public void CallToDonorPHone(String phone, String donorId) {

        String userId = "";

        try {
            JSONObject user = new JSONObject(pref.getUserData());

            userId = user.getString("id");

        } catch (JSONException e) {
            e.printStackTrace();
        }

        saveCall(phone, userId, donorId);

    }


    public void saveCall(final String mobile, final String call_from, final String call_to) {

        RequestQueue queue = Volley.newRequestQueue(this);

        StringRequest request = new StringRequest(Request.Method.POST, UrlHelper.saveCallUrl,
                response -> {
                    JSONObject jsonObject;
                    try {

                        Log.e(TAG, "onResponse: " + response);

                        CallToPHone(mobile);

                        jsonObject = new JSONObject(response);
                        JSONArray jsonArray = jsonObject.getJSONArray(Constant.TAG);

                        if (jsonArray.getJSONObject(0).getBoolean("success")) {

                            Toast.makeText(MainActivity.this, "Call Saved!", Toast.LENGTH_SHORT).show();

                        }

                    } catch (Exception ee) {
                        ee.printStackTrace();
                    }
                },
                error -> {
                    Log.d(TAG, "onErrorResponse: error: " + error.toString());
                    CallToPHone(call_to);
                }
        ) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("call_to", call_to);
                params.put("call_from", call_from);

                return params;
            }
        };

        queue.add(request);
    }


    public void rateCall(final ItemCallRate item, final String fragmentName) {

        RequestQueue queue = Volley.newRequestQueue(this);

        StringRequest request = new StringRequest(Request.Method.POST, UrlHelper.rateCallUrl,
                response -> {
                    JSONObject jsonObject;
                    try {

                        Log.e(TAG, "onResponse: " + response);
                        jsonObject = new JSONObject(response);
                        JSONArray jsonArray = jsonObject.getJSONArray(Constant.TAG);

                        if (jsonArray.getJSONObject(0).getBoolean("success")) {

                            Toast.makeText(MainActivity.this, "Success", Toast.LENGTH_SHORT).show();
                        } else {

                        }

                    } catch (Exception ee) {
                        ee.printStackTrace();
                    }
                },
                error -> Log.d(TAG, "onErrorResponse: error: " + error.toString())
        ) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("id", item.getId());
                params.put("subject", item.getSubject());
                params.put("feedback", item.getFeedback());
                params.put("donatedOrNot", item.getDonated());
                return params;
            }
        };

        queue.add(request);
    }


    public void confirmRequest(final String id, String shortMsg, final String what, final String type) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Are you sure you want to " + shortMsg + "?");
        builder.setPositiveButton("Yes, " + what, (dialog, which) -> sendUpdateRequest(type, id, what));
        builder.setNegativeButton("No", (dialog, which) -> dialog.dismiss());
        builder.show();

    }

    public void sendUpdateRequest(final String requestType, final String id, final String what) {

        RequestQueue queue = Volley.newRequestQueue(this);

        StringRequest request = new StringRequest(Request.Method.POST, UrlHelper.updateRequest,
                response -> {
                    JSONObject jsonObject;
                    try {

                        Constant.asdf(TAG + " onResponse: " + response);

                        jsonObject = new JSONObject(response);
                        JSONArray jsonArray = jsonObject.getJSONArray(Constant.TAG);

                        if (jsonArray.getJSONObject(0).getBoolean("success")) {
                            Toast.makeText(MainActivity.this, "Success", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(MainActivity.this, "Failed", Toast.LENGTH_SHORT).show();
                        }

                    } catch (Exception ee) {
                        ee.printStackTrace();
                    }
                },
                error -> Log.d(TAG, "onErrorResponse: error: " + error.toString())
        ) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();

                params.put("id", id);
                params.put("type", requestType);
                params.put("what", what);

                return params;
            }
        };

        queue.add(request);
    }

    public void signOut() {
        PrefManager pref = new PrefManager(this);
        pref.clearPref();

        FirebaseAuth.getInstance().signOut();
        finish();
        startActivity(new Intent(this, SplashActivity.class));
    }


}
