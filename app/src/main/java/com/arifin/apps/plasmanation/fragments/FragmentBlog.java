package com.arifin.apps.plasmanation.fragments;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.OvershootInterpolator;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.SearchView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.arifin.apps.plasmanation.R;
import com.arifin.apps.plasmanation.adapters.AdapterBlog;
import com.arifin.apps.plasmanation.items.ItemBlog;
import com.arifin.apps.plasmanation.utils.Constant;
import com.arifin.apps.plasmanation.utils.Methods;
import com.arifin.apps.plasmanation.utils.UrlHelper;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import jp.wasabeef.recyclerview.adapters.AnimationAdapter;
import jp.wasabeef.recyclerview.adapters.ScaleInAnimationAdapter;

import static com.arifin.apps.plasmanation.utils.Constant.TAG;

public class FragmentBlog extends Fragment {

    private RecyclerView recyclerView;
    private AdapterBlog adapterBlog;
    private ArrayList<ItemBlog> arrayList = new ArrayList<>();
    private ProgressBar progressBar;
    private TextView textView_empty;
    Methods methods;

    private SearchView searchView;
    private final SearchView.OnQueryTextListener queryTextListener = new SearchView.OnQueryTextListener() {
        @Override
        public boolean onQueryTextSubmit(String s) {

            return true;
        }

        @Override
        public boolean onQueryTextChange(String s) {
            if (!searchView.isIconified()) {
                Constant.searchString = s;
                adapterBlog.getFilter().filter(s);
                adapterBlog.notifyDataSetChanged();
            }
            return false;
        }
    };

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_blog, container, false);

        methods = new Methods(getActivity());
        methods.loadBanners(rootView.findViewById(R.id.adMobView));

        arrayList = new ArrayList<>();
        progressBar = rootView.findViewById(R.id.pb_cat);
        progressBar.setVisibility(View.GONE);
        textView_empty = rootView.findViewById(R.id.tv_empty_cat);
        recyclerView = rootView.findViewById(R.id.rv_blood_donors);

        LinearLayoutManager linear = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(linear);
        recyclerView.setAdapter(adapterBlog);


        getBlogsData();
        setAdapter();

        setHasOptionsMenu(true);
        return rootView;
    }

    private void getBlogsData() {
        RequestQueue queue = Volley.newRequestQueue(getActivity());
        StringRequest request = new StringRequest(Request.Method.POST, UrlHelper.blogsUrl,
                response -> {
                    JSONObject jsonObject;
                    try {
                        progressBar.setVisibility(View.GONE);

                        Log.d(TAG, "onResponse: " + response);
                        jsonObject = new JSONObject(response);
                        JSONArray jsonArray = jsonObject.getJSONArray("Blogs");
                        JSONObject c;

                        Gson gson = new Gson();
                        Type type = new TypeToken<List<ItemBlog>>() {
                        }.getType();

                        arrayList = new ArrayList<>();

                        arrayList = gson.fromJson(jsonArray.toString(), type);

                        setAdapter();
                    } catch (Exception ee) {
                        ee.printStackTrace();
                    }
                },
                error -> {

                }
        ) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                return params;
            }
        };

        queue.add(request);
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_search, menu);

        MenuItem item = menu.findItem(R.id.menu_search);
        item.setShowAsAction(MenuItem.SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW | MenuItem.SHOW_AS_ACTION_IF_ROOM);

        searchView = (SearchView) menu.findItem(R.id.menu_search).getActionView();
        searchView.setOnQueryTextListener(queryTextListener);

        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
        }
        return super.onOptionsItemSelected(item);
    }

    private void setAdapter() {

        adapterBlog = new AdapterBlog(getActivity(), arrayList);
        AnimationAdapter adapterAnim = new ScaleInAnimationAdapter(adapterBlog);
        adapterAnim.setFirstOnly(true);
        adapterAnim.setDuration(500);
        adapterAnim.setInterpolator(new OvershootInterpolator(.9f));
        recyclerView.setAdapter(adapterAnim);
        adapterBlog.notifyDataSetChanged();
        setExmptTextView();
    }

    private void setExmptTextView() {
        if (arrayList.size() == 0) {
            textView_empty.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
        } else {
            recyclerView.setVisibility(View.VISIBLE);
            textView_empty.setVisibility(View.GONE);
        }
    }

    @Override
    public void onResume() {
        if (getActivity() != null)
            getActivity().setTitle(getResources().getString(R.string.title_blog));
        super.onResume();
    }

}
