package com.arifin.apps.plasmanation.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.arifin.apps.plasmanation.R;
import com.arifin.apps.plasmanation.activities.MainActivity;
import com.arifin.apps.plasmanation.items.ItemAbout;
import com.arifin.apps.plasmanation.utils.Constant;
import com.arifin.apps.plasmanation.utils.DBHelper;
import com.arifin.apps.plasmanation.utils.Methods;
import com.arifin.apps.plasmanation.utils.UrlHelper;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class FragmentAbout extends Fragment {

    private WebView      webView;
    private TextView     textView_appname;
    private TextView     textView_email;
    private TextView     textView_website;
    private TextView     textview_author;
    private TextView     textView_contact;
    private TextView     textView_version;
    private ImageView    imageView_logo;
    private LinearLayout ll_email;
    private LinearLayout ll_website;
    private LinearLayout ll_company;
    private LinearLayout ll_contact;
    private DBHelper     dbHelper;
    private Context      ctx;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_about, container, false);

        ctx = getActivity();
        handleAbout(rootView);

        return rootView;
    }

    private void handleAbout(View v) {
        dbHelper = new DBHelper(ctx);
        Methods methods = new Methods(ctx);
        methods.setStatusColor(((MainActivity) ctx).getWindow());
        methods.forceRTLIfSupported(((MainActivity) ctx).getWindow());

        ProgressDialog pbar = new ProgressDialog(ctx);
        pbar.setMessage(getResources().getString(R.string.loading));
        pbar.setCancelable(false);

        webView          = v.findViewById(R.id.webView);
        textView_appname = v.findViewById(R.id.textView_about_appname);
        textView_email   = v.findViewById(R.id.textView_about_email);
        textView_website = v.findViewById(R.id.textView_about_site);
        textview_author  = v.findViewById(R.id.textView_author);
        textView_contact = v.findViewById(R.id.textView_about_contact);
        textView_version = v.findViewById(R.id.textView_about_appversion);
        imageView_logo   = v.findViewById(R.id.imageView_about_logo);

        ll_email   = v.findViewById(R.id.ll_email);
        ll_website = v.findViewById(R.id.ll_website);
        ll_contact = v.findViewById(R.id.ll_contact);
        ll_company = v.findViewById(R.id.ll_company);

        if (methods.isNetworkAvailable()) {

            RequestQueue queue = Volley.newRequestQueue(ctx);
            StringRequest request = new StringRequest(Request.Method.POST, UrlHelper.settingsUrl,
                    response -> {
                        JSONObject jsonObject;
                        try {
                            jsonObject = new JSONObject(response);
                            JSONArray  jsonArray = jsonObject.getJSONArray(Constant.TAG);
                            JSONObject c;
                            for (int i = 0; i < jsonArray.length(); i++) {
                                c = jsonArray.getJSONObject(i);

                                String appname     = c.getString("app_name");
                                String applogo     = c.getString("app_logo");
                                String desc        = c.getString("app_description");
                                String appversion  = c.getString("app_version");
                                String appauthor   = c.getString("app_author");
                                String appcontact  = c.getString("app_contact");
                                String email       = c.getString("app_email");
                                String website     = c.getString("app_website");
                                String privacy     = c.getString("app_privacy_policy");
                                String developedby = c.getString("app_developed_by");
                                Constant.ad_banner_id    = c.getString("banner_ad_id");
                                Constant.ad_inter_id     = c.getString("interstital_ad_id");
                                Constant.isBannerAd      = Boolean.parseBoolean(c.getString("banner_ad"));
                                Constant.isInterAd       = Boolean.parseBoolean(c.getString("interstital_ad"));
                                Constant.ad_publisher_id = c.getString("publisher_id");
                                Constant.adShow          = Integer.parseInt(c.getString("interstital_ad_click"));

                                Constant.itemAbout = new ItemAbout(appname, applogo, desc, appversion, appauthor, appcontact, email, website, privacy, developedby);
                                setVariables();
                                dbHelper.addtoAbout();
                            }
                        } catch (Exception ee) {
                            ee.printStackTrace();
                        }
                    },
                    error -> {

                    }
            ) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<>();
                    params.put("", "");
                    return params;
                }
            };

            queue.add(request);
        } else {
            if (dbHelper.getAbout()) {
                setVariables();
            }
        }
    }

    private void setVariables() {

        String appname    = Constant.itemAbout.getAppName();
        String applogo    = Constant.itemAbout.getAppLogo();
        String desc       = Constant.itemAbout.getAppDesc();
        String appversion = Constant.itemAbout.getAppVersion();
        String appauthor  = Constant.itemAbout.getAuthor();
        String appcontact = Constant.itemAbout.getContact();
        String email      = Constant.itemAbout.getEmail();
        String website    = Constant.itemAbout.getWebsite();

        textView_appname.setText(appname);
        if (!email.trim().isEmpty()) {
            ll_email.setVisibility(View.VISIBLE);
            textView_email.setText(email);
        }

        if (!website.trim().isEmpty()) {
            ll_website.setVisibility(View.VISIBLE);
            textView_website.setText(website);
        }

        if (!appauthor.trim().isEmpty()) {
            ll_company.setVisibility(View.VISIBLE);
            textview_author.setText(appauthor);
        }

        if (!appcontact.trim().isEmpty()) {
            ll_contact.setVisibility(View.VISIBLE);
            textView_contact.setText(appcontact);
        }

        if (!appversion.trim().isEmpty()) {
            textView_version.setText(appversion);
        }
        if (applogo.trim().isEmpty()) {
            imageView_logo.setVisibility(View.GONE);
        } else {
            Picasso.get()
                    .load(UrlHelper.imageUrl + applogo)
                    .into(imageView_logo);
        }

        String content = "<?xml version='1.0' encoding='UTF-8' ?>" +
                "<html><head>" +
                "<meta http-equiv='content-type' content='text/html; charset=utf-8' />" +
                "</head><body>" + Constant.itemAbout.getAppDesc() + "</body></html>";
        String encodedHtml = Base64.encodeToString(content.getBytes(), Base64.NO_PADDING);
        webView.loadData(encodedHtml, "text/html", "base64");
    }
}
