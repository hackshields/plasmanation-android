package com.arifin.apps.plasmanation.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Point;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.RelativeLayout;

import androidx.appcompat.app.AppCompatActivity;

import com.arifin.apps.plasmanation.R;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;

import java.util.Random;
//import com.google.android.gms.ads.AdListener;
//import com.google.android.gms.ads.AdRequest;
//import com.google.android.gms.ads.AdSize;
//import com.google.android.gms.ads.AdView;
//import com.google.android.gms.ads.InterstitialAd;
//import com.google.android.gms.ads.MobileAds;


public class Methods {

    private final Context                  context;
    private       SharedPreferences.Editor editor;
    private       int                      countAds;
    private       InterstitialAd           mInterstitialAd;

    public Methods(boolean ad, Context context) {
        this.context = context;

        if (ad) {
            MobileAds.initialize(context, Constant.admob_app_id);
        }
    }

    // constructor
    public Methods(Context context) {
        this.context = context;
    }

    public boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public int getScreenWidth(Context ctx) {
        int columnWidth;

        final Point point = new Point();

        DisplayMetrics displaymetrics = new DisplayMetrics();
        ((AppCompatActivity) ctx).getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);

        point.x = displaymetrics.widthPixels;
        point.y = displaymetrics.heightPixels;

        columnWidth = point.x;
        return columnWidth;
    }

    public void forceRTLIfSupported(Window window) {
        if (context.getResources().getString(R.string.isRTL).equals("true")) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                window.getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
            }
        }
    }

    public void setStatusColor(Window window) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
            window.setStatusBarColor(Color.TRANSPARENT);
        }
    }

    public void loadInterAds(final Context context) {

        if (Constant.isInterAd) {
            if (isNetworkAvailable()) {
                Log.d(Constant.TAG, "loadInterAds: ");
                SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
                editor = prefs.edit();
                int totalCount = prefs.getInt("counter", 0);
                countAds = prefs.getInt("number", 0);

                mInterstitialAd = new InterstitialAd(context);
//        mInterstitialAd.setAdUnitId(context.getString(R.string.interstitial_ad_id));
                mInterstitialAd.setAdUnitId(Constant.ad_inter_id);
                mInterstitialAd.loadAd(new AdRequest.Builder().addTestDevice("1E9A57F2A52918F7925922BBB40A7011").build());
                mInterstitialAd.setAdListener(new AdListener() {
                    @Override
                    public void onAdClosed() {
                        editor.putInt("number", ++countAds);
                        editor.apply();
                        countAdsMethod(context);
                        requestNewInterstitial(context);
                    }
                });
            }
        }

    }

    private void countAdsMethod(Context context) {
        Log.d(Constant.TAG, "countAdsMethod:");
        //define a new Random class
        Random r = new Random();

        //minimum number to generate as random number
        int minNumber = Integer.parseInt(context.getString(R.string.ad_closemin_count));

        //maximum number to generate as random number
        int maxNumber = Integer.parseInt(context.getString(R.string.ad_closemax_count));

        //get the next random number within range
        int randomNumber = r.nextInt((maxNumber - minNumber) + minNumber) + minNumber;

        if (+countAds >= (randomNumber)) {
            countAds = 0;
            editor.putInt("number", 0);
            editor.commit();
//            showDialog();
        }
    }

    private void requestNewInterstitial(Context context) {
        Log.d(Constant.TAG, "requestNewInterstitial: ");

        AdRequest adRequest = new AdRequest.Builder()
                .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                .addTestDevice("1E9A57F2A52918F7925922BBB40A7011")
                .build();
        mInterstitialAd.loadAd(adRequest);
    }

    public void showInterAd(final Context context) {

        if (Constant.isInterAd) {
            if (isNetworkAvailable()) {
                Log.d(Constant.TAG, "showInterAd: ");
                new android.os.Handler().postDelayed(() -> {
                    Constant.adsCount++;
                    if (Constant.adsCount == Constant.showAdAfterCount) {
                        Constant.adsCount = 0;
                        if (mInterstitialAd.isLoaded()) {
//                            if (!BuildConfig.DEBUG)
                            mInterstitialAd.show();
//                            else
//                                Toast.makeText(context, "Debug Mode, It's Interstitial Ad", Toast.LENGTH_SHORT).show();
                        } else {
                            Log.d("TAG", "The interstitial wasn't loaded yet.");
                            requestNewInterstitial(context);
                        }
                    }
                }, 2500);
            }
        }
    }

    public void loadBanners(final View view) {

        try {
            if (Constant.isBannerAd) {
                if (isNetworkAvailable()) {
                    final AdView mAdView = new AdView(context);
                    mAdView.setAdSize(AdSize.SMART_BANNER);
                    mAdView.setAdUnitId(Constant.ad_banner_id);

                    Log.d(Constant.TAG, "loadBanners: ");
                    AdRequest adRequest = new AdRequest.Builder()
                            .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                            .addTestDevice("EE5B60C7671726EDC72F3618D03FFA0C")
                            .build();
                    //            if (mAdView.getAdSize() != null || mAdView.getAdUnitId() != null)
                    mAdView.loadAd(adRequest);

                    mAdView.setAdListener(new AdListener() {
                        @Override
                        public void onAdFailedToLoad(int i) {
                            super.onAdFailedToLoad(i);

                            Log.d(Constant.TAG, "onAdFailedToLoad: ");
//                        ((RelativeLayout) view).addView(mAdView);
//                        ((RelativeLayout) view).addView(mAdView);
                            view.setVisibility(View.GONE);
//                        ((RelativeLayout) view).addView(mAdView);

                            new Handler().postDelayed(() -> loadBanners(view), 5000);

                        }

                        @Override
                        public void onAdLoaded() {
                            super.onAdLoaded();
                            Log.d(Constant.TAG, "onAdLoaded: ");
                            ((RelativeLayout) view).removeAllViews();
                            ((RelativeLayout) view).addView(mAdView);
                            view.setVisibility(View.VISIBLE);
                        }
                    });
                } else {
                    view.setVisibility(View.GONE);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static boolean isEmulator() {
        return Build.FINGERPRINT.startsWith("generic")
                || Build.FINGERPRINT.startsWith("unknown")
                || Build.MODEL.contains("google_sdk")
                || Build.MODEL.contains("Emulator")
                || Build.MODEL.contains("Android SDK built for x86")
                || Build.MANUFACTURER.contains("Genymotion")
                || (Build.BRAND.startsWith("generic") && Build.DEVICE.startsWith("generic"))
                || "google_sdk".equals(Build.PRODUCT);
    }


    public static boolean check_internet(Context context) {
        ConnectionDetector cd                = new ConnectionDetector(context);
        return cd.isConnectingToInternet();
    }

    public static boolean check_gps(Context context) {
        GPSTracker gps = new GPSTracker(context);
        // check if GPS enabled
        if (gps.canGetLocation()) {
            try {

                Constant.curr_latitude  = gps.getLatitude();
                Constant.curr_longitude = gps.getLongitude();

                gps.stopUsingGPS();

            } catch (NullPointerException e) {

            } catch (NumberFormatException e) {

            }
        } else {
            if (!Constant.isAlertShowing) {
                gps.showSettingsAlert();
            }
        }
        return gps.canGetLocation();
    }


}
