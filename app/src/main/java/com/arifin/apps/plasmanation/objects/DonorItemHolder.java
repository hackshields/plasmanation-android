package com.arifin.apps.plasmanation.objects;

import java.io.Serializable;

public class DonorItemHolder implements Serializable {

    String id = "";
    String fullName = "";
    String mobile = "";
    String address = "";
    String dateOfBirth = "";
    String bloodGroup = "";
    String country = "";
    String state = "";
    String city = "";
    String habits = "";
    String type = "";
    String isReminder = "";
    String lastDonationDate = "";
    String points = "";
    String status = "";
    String latitude = "";
    String longitude = "";
    String location = "";
    String views = "";
    String addedBy = "";
    String created = "";
    String countryName = "";
    String stateName = "";
    String cityName = "";
    String distance = "";
    String timeAgo = "";
    String kmDistance = "";

    public DonorItemHolder() {
        this.id = "";
        this.fullName = "";
        this.mobile = "";
        this.address = "";
        this.dateOfBirth = "";
        this.bloodGroup = "";
        this.country = "";
        this.state = "";
        this.city = "";
        this.habits = "";
        this.type = "";
        this.isReminder = "";
        this.lastDonationDate = "";
        this.points = "";
        this.status = "";
        this.latitude = "";
        this.longitude = "";
        this.location = "";
        this.views = "";
        this.addedBy = "";
        this.created = "";
        this.countryName = "";
        this.stateName = "";
        this.cityName = "";
        this.distance = "";
        this.timeAgo = "";
        this.kmDistance = "";
    }


}
